# Remarkable-Linux

> Remarkable is a powerful markdown editor available for Linux. It is fully featured and customizable. You can download the Linux version for free (or donate if you wish). Easy installers are available for Debian, Ubuntu, Fedora, SUSE and Arch systems.

Remarkable is in development, this repository aims to distribute the source code already developed. For the original repository go [here](https://github.com/remarkableapp/Remarkable-Linux).

## Screenshots

![Main Screenshot](Screenshots/main_screenshot.png)

![Syntax Highlighting](Screenshots/syntax_highlighting.png)

## Features
- Live Preview
- Github Markdown
- Export to PDF and HTML
- Custom CSS
- Syntax Highlighting
- Completely Customizable
- MathJax Support
- Keyboard Shortcuts


## Installing
### Linux
Remarkable is available for Debian, Ubuntu, Fedora, SUSE and Arch systems.
Check the download [page](https://remarkableapp.github.io/linux/download.html) or the [installers](https://github.com/krikmo/Remarkable/blob/master/Installers) folder.
### Windows
The Windows version is currently under development and will be ready soon!

## License

Copyright (c) 2016 Jamie McGowan <remarkableapp@outlook.com>

Remarkable is free software. Read the [full license](https://github.com/krikmo/Remarkable/blob/master/LICENSE.md)

## Relevant links:
[Original repository](https://github.com/remarkableapp/Remarkable-Linux/)

[Issues](https://github.com/remarkableapp/Remarkable-Linux/issues)

[Pull requests](https://github.com/remarkableapp/Remarkable-Linux/pulls)

[Changelog](https://github.com/krikmo/Remarkable/blob/master/CHANGELOG.md)

[Website](https://remarkableapp.github.io)

##### **This summary was written using Remarkable.**
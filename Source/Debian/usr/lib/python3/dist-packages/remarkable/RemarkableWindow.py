#!usr/bin/python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2015 <Jamie McGowan> <jamiemcgowan.dev@gmail.com>
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
### END LICENSE

from bs4 import BeautifulSoup
from gi.repository import Gtk # pylint: disable=E0611
from gi.repository import Gdk
from gi.repository import Pango
from gi.repository import WebKit
from locale import gettext as _
from urllib.request import urlopen
import markdown
import pdfkit
import re, subprocess, datetime, os, webbrowser, _thread, sys, locale
import tempfile
import traceback
import undobuffer
import unicodedata
import warnings

#Check if gtkspellcheck is installed
try:
    from gtkspellcheck import SpellChecker
    spellcheck_enabled = True
except:
    print("*Spellchecking not enabled.\n*To enable spellchecking install pygtkspellcheck\n*https://pypi.python.org/pypi/pygtkspellcheck/")
    spellcheck_enabled = False

import logging
logger = logging.getLogger('remarkable')

#Ignore warnings re. scroll handler (temp. fix) && starting GTK warning
warnings.filterwarnings("ignore", ".*has no handler with id.*")

from remarkable_lib import Window
from remarkable.AboutRemarkableDialog import AboutRemarkableDialog
from remarkable.PreferencesRemarkableDialog import PreferencesRemarkableDialog

app_version = 1.62 #Remarkable app version

class RemarkableWindow(Window):
    __gtype_name__ = "RemarkableWindow"
    
    def finish_initializing(self, builder): # pylint: disable=E1002
        """Set up the main window"""
        super(RemarkableWindow, self).finish_initializing(builder)

        self.AboutDialog = AboutRemarkableDialog
        self.PreferencesDialog = PreferencesRemarkableDialog

        self.settings = Gtk.Settings.get_default()

        self.is_fullscreen = False
        self.editor_position = 0
        self.homeDir = os.environ['HOME']
        self.path = os.path.join(self.homeDir, ".remarkable/")
        self.settings_path = os.path.join(self.path, "remarkable.settings")

        self.name = "Untitled" #Title of the current file, set to 'Untitled' as default

        #CSS declarations go here---- <Embedded here as alternative to tracking location of external files>
        self.dark = 'body{background:#000;font-family:Georgia,Palatino,serif;color:#EEE;line-height:1;padding:30px;margin:auto;max-width:42em}h1,h2{text-align:center}h1,h2,h3,h4{font-weight:400}h1,h2,h3,h4,h5,p{margin-bottom:24px;padding:0}h1{font-size:48px}h2{font-size:36px;margin:24px 0 6px}h3{font-size:24px}h4{font-size:21px}h5{font-size:18px}a{color:#61BFC1;margin:0;padding:0;text-decoration:none;vertical-align:baseline}a:hover{text-decoration:underline}a:visited{color:#466B6C}ol,ul{padding:0;margin:0}li{line-height:24px}li ul{margin-left:24px}ol,p,ul{font-size:16px;line-height:24px;max-width:540px}pre{padding:0 24px;max-width:800px;white-space:pre-wrap}code{font-family:Consolas,Monaco,Andale Mono,monospace;line-height:1.5;font-size:13px}aside{display:block;float:right;width:390px}blockquote{border-left:.5em solid #eee;padding:0 2em;margin-left:0;max-width:476px}blockquote cite{font-size:14px;line-height:20px;color:#bfbfbf}blockquote p{color:#666;max-width:460px}hr{width:540px;text-align:left;margin:0 auto 0 0;color:#999}button,input,select,textarea{margin:0;vertical-align:baseline}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}button,input[type=button],input[type=reset],input[type=submit]{cursor:pointer;-webkit-appearance:button}input:not([type=image]),textarea{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}input[type=search]{-webkit-appearance:textfield;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}input[type=search]::-webkit-search-decoration{-webkit-appearance:none}input,label,select,textarea{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;font-weight:400;line-height:normal;margin-bottom:18px}input[type=checkbox],input[type=radio]{cursor:pointer;margin-bottom:0}input[type=password],input[type=text],select,textarea{display:inline-block;width:210px;padding:4px;font-size:13px;font-weight:400;line-height:18px;height:18px;color:gray;border:1px solid #ccc;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}input[type=file],select{height:27px;line-height:27px}textarea{height:auto}:-moz-placeholder{color:#bfbfbf}::-webkit-input-placeholder{color:#bfbfbf}input[type=password],input[type=text],select,textarea{-webkit-transition:border linear .2s,box-shadow linear .2s;-moz-transition:border linear .2s,box-shadow linear .2s;transition:border linear .2s,box-shadow linear .2s;-webkit-box-shadow:inset 0 1px 3px rgba(0,0,0,.1);-moz-box-shadow:inset 0 1px 3px rgba(0,0,0,.1);box-shadow:inset 0 1px 3px rgba(0,0,0,.1)}input[type=password]:focus,input[type=text]:focus,textarea:focus{outline:0;border-color:rgba(82,168,236,.8);-webkit-box-shadow:inset 0 1px 3px rgba(0,0,0,.1),0 0 8px rgba(82,168,236,.6);-moz-box-shadow:inset 0 1px 3px rgba(0,0,0,.1),0 0 8px rgba(82,168,236,.6);box-shadow:inset 0 1px 3px rgba(0,0,0,.1),0 0 8px rgba(82,168,236,.6)}button{display:inline-block;padding:4px 14px;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:13px;line-height:18px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;-webkit-box-shadow:inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);-moz-box-shadow:inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);box-shadow:inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);background-color:#0064cd;background-repeat:repeat-x;background-image:-khtml-gradient(linear,left top,left bottom,from(#049cdb),to(#0064cd));background-image:-moz-linear-gradient(top,#049cdb,#0064cd);background-image:-ms-linear-gradient(top,#049cdb,#0064cd);background-image:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#049cdb),color-stop(100%,#0064cd));background-image:-webkit-linear-gradient(top,#049cdb,#0064cd);background-image:-o-linear-gradient(top,#049cdb,#0064cd);background-image:linear-gradient(top,#049cdb,#0064cd);color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,.25);border:1px solid #004b9a;-webkit-transition:.1s linear all;-moz-transition:.1s linear all;transition:.1s linear all;border-color:#0064cd #0064cd #003f81;border-color:rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25)}button:hover{color:#fff;background-position:0 -15px;text-decoration:none}button:active{-webkit-box-shadow:inset 0 3px 7px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05);-moz-box-shadow:inset 0 3px 7px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05);box-shadow:inset 0 3px 7px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05)}button::-moz-focus-inner{padding:0;border:0}mark{background-color:#ff69b4}table{border-collapse:collapse}td,th{border:1px solid #fff;padding:.5rem;text-align:left}'
        self.foghorn = '@import url(http://fonts.googleapis.com/css?family=Vollkorn:400,400italic,700,700italic&subset=latin);body,html{padding:1em;margin:auto;background:#fff}body{font:1.3em Vollkorn,Palatino,Times;color:#333;line-height:1;text-align:justify}article,footer,header,nav{margin:0 auto}article{margin-top:4em;margin-bottom:4em;min-height:400px}footer{margin-bottom:50px}video{margin:2em 0;border:1px solid #ddd}nav{font-size:.9em;font-style:italic;border-bottom:1px solid #ddd;padding:1em 0}nav p{margin:0}h1{margin-top:0;font-weight:400;text-align:center}h2{font-weight:400;text-align:center}h3{font-weight:400;font-style:italic}p{margin-top:0;-webkit-hypens:auto;-moz-hypens:auto;hyphens:auto}ul{list-style:square;padding-left:1.2em}ol{padding-left:1.2em}blockquote{margin-left:1em;padding-left:1em;border-left:1px solid #ddd}code{font-family:Consolas,Menlo,Monaco,monospace,serif;font-size:.9em;background:#fff}a{color:#2484c1;text-decoration:none}a:hover{text-decoration:underline}a img{border:0}h1 a,h1 a:hover{color:#333;text-decoration:none}hr{color:#ddd;height:1px;margin:2em 0;border-top:solid 1px #ddd;border-bottom:0;border-left:0;border-right:0}p#heart{font-size:2em;line-height:1;text-align:center;color:#ccc}.red{color:#b50000}body#index li{margin-bottom:1em}@media only screen and (max-device-width:1024px){body{font-size:120%;line-height:1.4}}@media only screen and (max-device-width:480px){body{text-align:left}article,footer{width:auto}article{padding:0 10px}}table{padding:0}table tr{border-top:1px solid #ccc;background-color:#fff;margin:0;padding:0}table tr:nth-child(2n){background-color:#aaa}table tr th{font-weight:700;border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td{border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td :first-child,table tr th :first-child{margin-top:0}table tr td:last-child,table tr th :last-child{margin-bottom:0}img{max-width:100%}'
        self.github = 'body{font:16px Helvetica,Arial,sans-serif;line-height:1.4;color:#333;word-wrap:break-word;background-color:#fff;padding:10px 15px}strong{font-weight:700}h1{font-size:2em;margin:.67em 0;text-align:center}h2{font-size:1.75em}h3{font-size:1.5em}h4{font-size:1.25em}h1,h2,h3,h4,h5,h6{font-weight:700;position:relative;margin-top:15px;margin-bottom:15px;line-height:1.1}h1,h2{border-bottom:1px solid #eee}hr{height:0;margin:15px 0;overflow:hidden;background:0 0;border:0;border-bottom:1px solid #ddd}a{color:#4183C4}a.absent{color:#c00}ol,ul{padding-left:15px;margin-left:5px}ol{list-style-type:lower-roman}table{padding:0}table tr{border-top:1px solid #ccc;background-color:#fff;margin:0;padding:0}table tr:nth-child(2n){background-color:#aaa}table tr th{font-weight:700;border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td{border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td :first-child,table tr th :first-child{margin-top:0}table tr td:last-child,table tr th :last-child{margin-bottom:0}img{max-width:100%}code{padding:0 5px;background-color:#d3d3d3}blockquote{padding: 0 15px;border-left:4px solid #ccc}'
        self.handwriting_css = '@import url(http://fonts.googleapis.com/css?family=Indie+Flower);body{font-family:Indie Flower,Daniel,cursive}h1,h2{text-align:center}blockquote{margin:1.5em 10px;padding:.5em 10px}blockquote:before{color:#000;content:open-quote;font-size:2em;line-height:.1em;margin-right:.25em;vertical-align:-.4em}blockquote:after{color:#000;content:close-quote;font-size:2em;line-height:.1em;margin-left:.25em;vertical-align:-.4em}blockquote p{display:inline}hr{border:0;border-top:dashed 2px gray}table{padding:0}table tr{border-top:1px solid #ccc;background-color:#fff;margin:0;padding:0}table tr:nth-child(2n){background-color:#aaa}table tr th{font-weight:700;border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td{border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td :first-child,table tr th :first-child{margin-top:0}table tr td:last-child,table tr th :last-child{margin-bottom:0}'
        self.markdown = 'html{font-size:100%;overflow-y:scroll;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}body{color:#444;font-family:Georgia,Palatino,"Palatino Linotype",Times,"Times New Roman",serif;font-size:12px;line-height:1.5em;padding:1em;margin:auto;max-width:42em;background:#fefefe}a{color:#0645ad;text-decoration:none}a:visited{color:#0b0080}a:hover{color:#06e}a:active{color:#faa700}a:focus{outline:dotted thin}a:active,a:hover{outline:0}::-moz-selection{background:rgba(255,255,0,.3);color:#000}::selection{background:rgba(255,255,0,.3);color:#000}a::-moz-selection{background:rgba(255,255,0,.3);color:#0645ad}a::selection{background:rgba(255,255,0,.3);color:#0645ad}p{margin:1em 0}img{max-width:100%}h1,h2,h3,h4,h5,h6{font-weight:400;color:#111;line-height:1em}h4,h5,h6{font-weight:700}h1{font-size:2.5em}h2{font-size:2em}h3{font-size:1.5em}h4{font-size:1.2em}h5{font-size:1em}h6{font-size:.9em}blockquote{color:#666;margin:0;padding-left:3em;border-left:.5em #EEE solid}hr{display:block;height:2px;border:0;border-top:1px solid #aaa;border-bottom:1px solid #eee;margin:1em 0;padding:0}code,kbd,pre,samp{color:#000;font-family:monospace,monospace;font-size:.98em}pre{white-space:pre;white-space:pre-wrap;word-wrap:break-word}b,strong{font-weight:700}dfn{font-style:italic}ins{background:#ff9;color:#000;text-decoration:none}mark{background:#ff0;color:#000;font-style:italic;font-weight:700}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}ol,ul{margin:1em 0;padding:0 0 0 2em}li p:last-child{margin:0}dd{margin:0 0 0 2em}img{border:0;-ms-interpolation-mode:bicubic;vertical-align:middle}table{border-collapse:collapse;border-spacing:0}td{vertical-align:top}@media only screen and (min-width:480px){body{font-size:14px}}@media only screen and (min-width:768px){body{font-size:16px}}@media print{*{background:0 0!important;color:#000!important;filter:none!important;-ms-filter:none!important}body{font-size:12pt;max-width:100%}a,a:visited{text-decoration:underline}hr{height:1px;border:0;border-bottom:1px solid #000}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}.ir a:after,a[href^="javascript:"]:after,a[href^="#"]:after{content:""}blockquote,pre{border:1px solid #999;padding-right:1em;page-break-inside:avoid}img,tr{page-break-inside:avoid}img{max-width:100%!important}@page :left{margin:15mm 20mm 15mm 10mm}@page :right{margin:15mm 10mm 15mm 20mm}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}}table{padding:0}table tr{border-top:1px solid #ccc;background-color:#fff;margin:0;padding:0}table tr:nth-child(2n){background-color:#aaa}table tr th{font-weight:700;border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td{border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td :first-child,table tr th :first-child{margin-top:0}table tr td:last-child,table tr th :last-child{margin-bottom:0}'
        self.metro_vibes = '@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800);body{padding:20px;color:#8e8071;font-size:15px;font-family:"Open Sans",AppleSDGothicNeo-Medium,"Segoe UI","Malgun Gothic",sans-serif;background:#fff;-webkit-font-smoothing:antialiased}a{color:#3269a0}a:hover{color:#4183c4}h1,h2,h3,h4,h5{font-weight:400;color:#5c5146;letter-spacing:-1px}h2{border-bottom:1px solid #e6e6e6;line-height:1.7em}h6{color:#777}hr{border:1px solid #e6e6e6}p{line-height:19px}p>code{font-family:"Open Sans",AppleSDGothicNeo-Medium,"Segoe UI","Malgun Gothic",sans-serif;color:#e86741;font-size:.9em}pre>code{font-size:1em;font-family:"Open Sans",AppleSDGothicNeo-Medium,"Segoe UI","Malgun Gothic",sans-serif;letter-spacing:-1px;font-weight:400}blockquote{border-left:4px solid #e6e6e6;padding:0 15px;font-style:italic;color:#e86741}table{background-color:#fafafa}table tr td,table tr th{border:1px solid #e6e6e6}table tr:nth-child(2n){background-color:#f2f2f2}'
        self.metro_vibes_dark = '@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800);body{padding:20px;color:#fff;font-size:15px;font-family:"Open Sans",AppleSDGothicNeo-Medium,"Segoe UI","Malgun Gothic",sans-serif;background:#5c5146;-webkit-font-smoothing:antialiased}a{color:#e5b931}a:hover{color:#ebc85e}h1,h2,h3,h4,h5{font-weight:400;letter-spacing:-1px}h2{border-bottom:1px solid #796a5c;line-height:1.7em}h6{color:#777}hr{border:1px solid #3f3730}p{line-height:19px}p>code{font-family:"Open Sans",AppleSDGothicNeo-Medium,"Segoe UI","Malgun Gothic",sans-serif;color:#e86741;font-size:.9em}pre>code{font-size:1em;font-family:"Open Sans",AppleSDGothicNeo-Medium,"Segoe UI","Malgun Gothic",sans-serif;letter-spacing:-1px;font-weight:400}blockquote{border-left:4px solid #4e443b;padding:0 15px;font-style:italic;color:#e86741}table{background-color:#564c42}table tr td,table tr th{border:1px solid #3f3730}table tr:nth-child(2n){background-color:#4e443b}'
        self.modern_css = 'body{font-family:Helvetica,Arial,sans-serif;background-color:#fff}h1,h2{text-align:center;color:#6495ed}h3,h4{color:#6495ed}blockquote{background:#f9f9f9;border-left:10px solid #6495ed;margin:1.5em 10px;padding:.5em 10px;font-style:italic}blockquote p{display:inline}hr{clear:both;float:none;width:100%;height:2.5px;margin:1.4em 0;border:none;background:#ddd;background:#ddd -webkit-gradient(linear,left bottom,right bottom,color-stop(0,#fff),color-stop(0.1,#ddd),color-stop(0.9,#ddd),color-stop(1,#fff));background:#ddd -moz-linear-gradient(left center,#fff 0,#ddd 10%,#ddd 90%,#fff 100%)}table{padding:0}table tr{border-top:1px solid #ccc;background-color:#fff;margin:0;padding:0}table tr:nth-child(2n){background-color:#aaa}table tr th{font-weight:700;border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td{border:1px solid #ccc;text-align:left;margin:0;padding:6px 13px}table tr td :first-child,table tr th :first-child{margin-top:0}table tr td:last-child,table tr th :last-child{margin-bottom:0}'
        self.screen = 'html{font-size:62.5%}body{font-family:Helvetica,Arial,sans-serif;font-size:150%;line-height:1.3;color:#f6e6cc;width:700px;margin:auto;background:#27221a;position:relative;padding:0 30px}dl,ol,p,pre,table,ul{margin-bottom:1em}ul{margin-left:20px}a{text-decoration:none;cursor:pointer;color:#ba832c;font-weight:700}a:focus{outline:dotted 1px}a:focus,a:hover{color:#d3a459;text-decoration:none}a *,button *{cursor:pointer}hr{display:none}small{font-size:90%}button,input,option,select,textarea{font-family:Arial,"Lucida Grande","Lucida Sans Unicode",Arial,Verdana,sans-serif;font-size:100%}button,input[type=submit],label,option,select{cursor:pointer}.group:after{content:".";display:block;height:0;clear:both;visibility:hidden}* html .group{height:1%}.group{display:block}sup{font-size:80%;line-height:1;vertical-align:super}button::-moz-focus-inner{border:0;padding:1px}span.amp{font-family:Baskerville,"Goudy Old Style",Palatino,"Book Antiqua",serif;font-weight:400;font-style:italic;font-size:1.2em;line-height:.8}h1,h2,h3,h4,h5,h6{line-height:1.1;font-family:Baskerville,"Goudy Old Style",Palatino,"Book Antiqua",serif}h2{font-size:22pt}h3{font-size:20pt}h4{font-size:18pt}h5{font-size:16pt}h6{font-size:14pt}::selection{background:#745626}::-moz-selection{background:#745626}h1{font-size:420%;margin:0 0 .1em;font-family:Baskerville,"Goudy Old Style",Palatino,"Book Antiqua",serif}h1 a,h1 a:hover{color:#d7af72;font-weight:400;text-decoration:none}pre{background:rgba(0,0,0,.3);color:#fff;padding:8px 10px;border-radius:.4em;-moz-border-radius:.4em;-webkit-border-radius:.4em;overflow-x:hidden}pre code{font-size:10pt}.thumb{float:left;margin:10px}table{border-collapse:collapse}td,th{border:1px solid #f6e6cc;padding:.5rem;text-align:left}'
        self.solarized_dark = 'blockquote,body,h1,h2,h3,h4,h5,h6,html,img,li,ol,p,ul{margin:0;padding:0;font:inherit;vertical-align:baseline}html *{font-family:ff-din-web-pro-1,ff-din-web-pro-2,sans-serif;font-size:16px;line-height:19.2px;color-profile:sRGB}body{margin:40px 70px}p{font-weight:lighter;margin-bottom:20px}strong{font-weight:700}ol,ul{margin-left:2em;margin-bottom:20px}ol ol,ol ul,ul ol,ul ul{margin-top:10px}li{margin-bottom:3px}h1,h2,h3,h4,h5,h6{font-weight:lighter;text-transform:capitalize;margin-top:20px;margin-bottom:10px}h1,h2{font-size:24.62px;line-height:29.55px}h3{font-size:23.44px;line-height:28.13px}h4,h5,h6{font-size:22.16px;line-height:26.59px}img{margin-bottom:20px}h1 img,h2 img,h3 img,h4 img,h5 img,h6 img,p img{margin-bottom:0}pre{margin-bottom:20px}code,pre{font-family:monospace}pre{white-space:pre;white-space:pre-wrap;word-wrap:break-word;padding:15px}blockquote{border-left:4px solid;padding:0 15px}blockquote>:first-child{margin-top:0}blockquote>:last-child{margin-bottom:15px}h1{text-transform:uppercase;font-weight:700;border-bottom:1px solid}h2{border-bottom:1px solid}h3,h4,h5,h6{border-bottom:none}html *{color:#839496}html body{background-color:#002b36}html h1,html h2,html h3,html h4,html h5,html h6{border-color:#839496}html a:hover{background-color:#073642}html pre{color:#93a1a1;background-color:#073642}html a,html a:active,html a:visited,html code.url,html h1,html h2,html h3,html h4,html h5,html h6{color:#b58900}@media print{body{margin:0}*{color:#000!important}}table{border-collapse:collapse}td,th{border:1px solid #839496;padding:.5rem;text-align:left}mark{background-color:#93a1a1;color:#000;font-weight:700}'
        self.solarized_light = 'blockquote,body,h1,h2,h3,h4,h5,h6,html,img,li,ol,p,ul{margin:0;padding:0;font:inherit;vertical-align:baseline}html *{font-family:ff-din-web-pro-1,ff-din-web-pro-2,sans-serif;font-size:16px;line-height:19.2px;color-profile:sRGB}body{margin:40px 70px}p{font-weight:lighter;margin-bottom:20px}strong{font-weight:700}ol,ul{margin-left:2em;margin-bottom:20px}ol ol,ol ul,ul ol,ul ul{margin-top:10px}li{margin-bottom:3px}h1,h2,h3,h4,h5,h6{font-weight:lighter;text-transform:capitalize;margin-top:20px;margin-bottom:10px}h1,h2{font-size:24.62px;line-height:29.55px}h3{font-size:23.44px;line-height:28.13px}h4,h5,h6{font-size:22.16px;line-height:26.59px}img{margin-bottom:20px}h1 img,h2 img,h3 img,h4 img,h5 img,h6 img,p img{margin-bottom:0}pre{margin-bottom:20px}code,pre{font-family:monospace}pre{white-space:pre;white-space:pre-wrap;word-wrap:break-word;padding:15px}blockquote{border-left:4px solid;padding:0 15px}blockquote>:first-child{margin-top:0}blockquote>:last-child{margin-bottom:15px}h1{text-transform:uppercase;font-weight:700;border-bottom:1px solid}h2{border-bottom:1px solid}h3,h4,h5,h6{border-bottom:none}html *{color:#657b83}html body{background-color:#fdf6e3}html h1,html h2,html h3,html h4,html h5,html h6{border-color:#657b83}html a:hover{background-color:#eee8d5}html pre{color:#586e75;background-color:#eee8d5}html a,html a:active,html a:visited,html code.url,html h1,html h2,html h3,html h4,html h5,html h6{color:#b58900}@media print{body{margin:0}*{color:#000!important}}table{border-collapse:collapse}td,th{border:1px solid #657b83;padding:.5rem;text-align:left}mark{font-weight:700}'
        self.custom_css = ''

        self.css = self.github #Github is the default style (css) applied to the markdown
        #End of CSS declarations

        self.default_html_start = '<!doctype HTML><html><head><meta charset="utf-8"><title>Made with Remarkable!</title><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.1/styles/github.min.css">'
        self.default_html_start += "<style type='text/css'>" + self.css + "</style>"
        self.default_html_start += "</head><body id='MathPreviewF'>"
        self.default_html_end = '<script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.1/highlight.min.js"></script><script>hljs.initHighlightingOnLoad();</script><script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script><script type="text/javascript">MathJax.Hub.Config({"showProcessingMessages" : false,"messageStyle" : "none","tex2jax": { inlineMath: [ [ "$", "$" ] ] }});</script></body></html>'

        self.remarkable_settings = {}

        self.default_extensions = ['markdown.extensions.extra','markdown.extensions.toc', 'markdown.extensions.smarty', 'markdown.extensions.nl2br', 'markdown.extensions.urlize', 'markdown.extensions.Highlighting', 'markdown.extensions.Strikethrough', 'markdown.extensions.markdown_checklist', 'markdown.extensions.superscript', 'markdown.extensions.subscript', 'markdown.extensions.mathjax']
        self.safe_extensions = ['markdown.extensions.extra', 'markdown.extensions.nl2br']
        self.pdf_error_warning = False

        self.window = self.builder.get_object("remarkable_window")
        self.window.connect("delete-event", self.window_delete_event)
        self.window.connect("destroy", self.quit_requested)

        self.text_view = Gtk.TextView()
        self.text_buffer = undobuffer.UndoableBuffer()
        self.text_buffer.connect("changed",self.on_text_view_changed)
        self.text_view.set_buffer(self.text_buffer)
        self.text_view.set_wrap_mode(Gtk.WrapMode.WORD)

        self.live_preview = WebKit.WebView()
        self.live_preview.connect("console-message", self._javascript_console_message) # Suppress .js output
        self.live_preview.zoom_out()

        self.scrolledwindow_text_view = Gtk.ScrolledWindow()
        self.scrolledwindow_text_view.add(self.text_view)
        self.scrolledwindow_live_preview = Gtk.ScrolledWindow()
        self.scrolledwindow_live_preview.add(self.live_preview)

        self.paned = self.builder.get_object("paned")
        self.paned.set_position(self.window.get_size()[0]/2)
        self.paned.pack1(self.scrolledwindow_text_view)
        self.paned.pack2(self.scrolledwindow_live_preview)

        self.toolbar = self.builder.get_object("toolbar")
        self.toolbutton_undo = self.builder.get_object("toolbutton_undo")
        self.toolbutton_undo.set_sensitive(False)
        self.toolbutton_redo = self.builder.get_object("toolbutton_redo")
        self.toolbutton_redo.set_sensitive(False)

        self.statusbar = self.builder.get_object("statusbar")
        self.context_id = self.statusbar.get_context_id("main status bar")

        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        self.update_status_bar(self)
        self.update_live_preview(self)

        text = ""

        #Check if filename has been specified in terminal command
        if len(sys.argv) > 1:
            self.name = sys.argv[1]
            title = self.name.split("/")[-1]
            self.window.set_title("Remarkable: " + title)
            try:
                with open(sys.argv[1], 'r') as temp:
                    text = temp.read()
                    self.text_buffer.set_text(text)
                    self.text_buffer.set_modified(False)
            except:
                print(self.name + " does not exist, creating it")

        self.update_status_bar(self)
        self.update_live_preview(self)

        #Check if an updated version of application exists
        _thread.start_new_thread(self.check_for_updates, ())
        
        self.text_view.grab_focus()
        
        if spellcheck_enabled:
            try:
                self.spellchecker = SpellChecker(self.text_view, locale.getdefaultlocale()[0]) #Enabling spell checking
            except:
                pass #Spell checking not enabled

        self.tv_scrolled = self.scrolledwindow_text_view.get_vadjustment().connect("value-changed", self.scrollPreviewTo)
        self.lp_scrolled_fix = self.scrolledwindow_live_preview.get_vadjustment().connect("value-changed", self.scrollPreviewToFix)
        self.scrolledwindow_live_preview.get_vadjustment().set_lower(1)

        self.temp_file_list = []

    def check_settings(self):
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        if not os.path.isfile(self.settings_path):
            self.remarkable_settings = {}
            self.remarkable_settings['live-preview'] = True
            self.remarkable_settings['word-wrap'] = True        
            self.remarkable_settings['toolbar'] = True
            self.remarkable_settings['statusbar'] = True
            self.remarkable_settings['nightmode'] = False       
            self.remarkable_settings['css'] = '' 
            self.remarkable_settings['font'] = "Sans 10"  
            self.remarkable_settings['style'] = "github"             
            settings_file = open(self.settings_path, 'w')
            settings_file.write(str(self.remarkable_settings))
            settings_file.close()
        else:
            settings_file = open(self.settings_path)
            self.remarkable_settings = eval(settings_file.read())
            settings_file.close()
            self.load_settings()

    def write_settings(self):
        settings_file = open(self.settings_path, 'w')
        settings_file.write(str(self.remarkable_settings))
        settings_file.close()

    def load_settings(self):
        self.custom_css = self.remarkable_settings['css'] #Load the custom css (don't auto enable)

        if self.remarkable_settings['nightmode']:
            #Enable night/dark mode on startup
            self.builder.get_object("menuitem_night_mode").set_active(True)
            self.on_menuitem_night_mode_activate(self)

        if self.remarkable_settings['word-wrap'] == False:
            #Disable word wrap on startup
            self.builder.get_object("menuitem_word_wrap").set_active(False)
            self.on_menuitem_word_wrap_activate(self)

        if self.remarkable_settings['live-preview'] == False:
            #Disable Live Preview on startup
            self.builder.get_object("menuitem_live_preview").set_active(False)

        if self.remarkable_settings['toolbar'] == False:
            #Hide the toolbar on startup
            self.on_menuitem_toolbar_activate(self)

        if self.remarkable_settings['statusbar'] == False:
            #Hide the statusbar on startup
            self.on_menuitem_statusbar_activate(self)

        #Try to load the previously chosen font, may fail as font may not exist, ect.
        try:
            self.font = self.remarkable_settings['font']
            self.text_view.override_font(Pango.FontDescription(self.font))
        except:
            pass #Loading font failed --> leave at default font
            
        #Try to load the previously chosen style, may fail
        #so ignore if this is the case
        try:
            self.style = self.remarkable_settings['style']
            if self.style == "dark":
                self.css = self.dark
            elif self.style == "foghorn":
                self.css = self.foghorn
            elif self.style == "github":
                self.css = self.github
            elif self.style == "handwriting_css":
                self.css = self.handwriting_css
            elif self.style == "markdown":
                self.css = self.markdown
            elif self.style == "metro_vibes":
                self.css = self.metro_vibes
            elif self.style == "metro_vibes_dark":
                self.css = self.metro_vibes_dark
            elif self.style == "modern_css":
                self.css = self.modern_css
            elif self.style == "screen":
                self.css = self.screen
            elif self.style == "solarized_dark":
                self.css = self.solarized_dark
            elif self.style == "solarized_light":
                self.css = self.solarized_light
            elif self.style == "custom":
                self.css = self.custom_css
            else:
                print("Style key error")

            self.update_style(self)
            self.update_live_preview(self)
        except:
            print("Couldn't choose previously selected style")

    def scrollPreviewToFix(self, widget):
        self.scrolledwindow_live_preview.get_vadjustment().disconnect(self.lp_scrolled_fix)
        value = self.scrolledwindow_live_preview.get_vadjustment().get_value()
        if value == 0: #Fix
            self.scrollPreviewTo(self)
        else:
            pass #Something better?

    def scrollPreviewTo(self, widget):
        self.scrolledwindow_live_preview.get_vadjustment().disconnect(self.lp_scrolled_fix)
        value = self.scrolledwindow_text_view.get_vadjustment().get_value()
        upper_edit = self.scrolledwindow_text_view.get_vadjustment().get_upper()
        preview_upper = self.scrolledwindow_live_preview.get_vadjustment().get_upper()
        if value >= upper_edit - self.scrolledwindow_text_view.get_vadjustment().get_page_size():
            self.scrolledwindow_live_preview.get_vadjustment().set_value(preview_upper - self.scrolledwindow_live_preview.get_vadjustment().get_page_size())
        else:
            self.scrolledwindow_live_preview.get_vadjustment().set_value(value / upper_edit * preview_upper)
        self.lp_scrolled_fix = self.scrolledwindow_live_preview.get_vadjustment().connect("value-changed", self.scrollPreviewToFix)

    def on_menuitem_numbered_list_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            start_line = start.get_line()
            end_line = end.get_line()
            i = 1
            while (start_line <= end_line):
                temp_iter = self.text_buffer.get_iter_at_line(start_line)
                self.text_buffer.insert(temp_iter, str(i) + ". ")
                start_line += 1
                i += 1
        else:
            temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            line_number = temp_iter.get_line()
            start_iter = self.text_buffer.get_iter_at_line(line_number)
            self.text_buffer.insert(start_iter, "1. ")

    def on_menuitem_new_activate(self, widget):
        self.new(self)

    def on_toolbutton_new_clicked(self, widget):
        self.new(self)

    def new(self, widget):
        if self.check_for_save(None):
            self.save(self)
        else:
            start, end = self.text_buffer.get_bounds()
            self.text_buffer.delete(start, end)

        self.name = "Untitled"
        self.text_buffer.set_modified(False)
        self.window.set_title("Remarkable")

    def on_menuitem_open_activate(self, widget):
        self.open(self)

    def on_toolbutton_open_clicked(self, widget):
        self.open(self)

    def open(self, widget):
        start, end = self.text_buffer.get_bounds()
        text = self.text_buffer.get_text(start, end, False)
        if len(text) > 0:
            if self.check_for_save(None):
                self.save(self)
        self.window.set_sensitive(False)
        chooser = Gtk.FileChooserDialog(title="Open File", action=Gtk.FileChooserAction.OPEN, buttons=(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = chooser.run()
        if response == Gtk.ResponseType.OK:
            file = open(chooser.get_filename(), 'r')
            text = file.read()
            file.close()
            self.name = chooser.get_filename()
            self.text_buffer.set_text(text)
            title = chooser.get_filename().split("/")[-1]
            self.window.set_title("Remarkable: " + title)
            self.text_buffer.set_modified(False)
        elif response == Gtk.ResponseType.CANCEL:
            pass
        chooser.destroy()
        self.window.set_sensitive(True)

    def check_for_save(self, widget):
        reply = False
        if self.text_buffer.get_modified():
            message = "Do you want to save the changes you have made?"
            dialog = Gtk.MessageDialog(self.window,
                                       Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                       Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO,
                                       message)
            dialog.set_title("Save?")

            if dialog.run() == Gtk.ResponseType.NO:
                reply = False
            else:
                reply = True
            dialog.destroy()
        return reply

    def on_menuitem_save_activate(self, widget):
        self.save(self)

    def on_toolbutton_save_clicked(self, widget):
        self.save(self)

    def save(self, widget):
        if self.name != "Untitled":
            file = open(self.name, 'w')
            text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
            file.write(text)
            file.close()
            title = self.name.split("/")[-1]
            self.text_buffer.set_modified(False)
            self.window.set_title("Remarkable: " + title)
        else:
            self.save_as(self)

    def on_menuitem_save_as_activate(self, widget, crap = ""):
        self.save_as(self)

    def save_as(self, widget):
        self.window.set_sensitive(False)
        chooser = Gtk.FileChooserDialog(title=None, action=Gtk.FileChooserAction.SAVE, buttons=(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
        chooser.set_do_overwrite_confirmation(True)
        title = self.name.split("/")[-1]
        chooser.set_title("Save As: " + title)
        response = chooser.run()
        if response == Gtk.ResponseType.OK:
            file = open(chooser.get_filename(), 'w')
            self.name = chooser.get_filename()
            text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
            file.write(text)
            file.close()
            self.text_buffer.set_modified(False)
            title = self.name.split("/")[-1]
            self.window.set_title("Remarkable: " + title)
        elif response == Gtk.ResponseType.CANCEL:
            pass
        chooser.destroy()
        self.window.set_sensitive(True)

    def on_menuitem_export_html_activate(self, widget):
        self.window.set_sensitive(False)
        start, end = self.text_buffer.get_bounds()
        text = self.text_buffer.get_text(start, end, False)
        text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
        try:
            html_middle = markdown.markdown(text, self.default_extensions)
        except:
            try:
                html_middle = markdown.markdown(text, extensions =self.safe_extensions)
            except:
                html_middle = markdown.markdown(text)
        html = self.default_html_start + html_middle + self.default_html_end
        self.save_html(html)

    def on_menuitem_export_html_plain_activate(self, widget):
        #This can be re-factored
        self.window.set_sensitive(False)
        start, end = self.text_buffer.get_bounds()
        text = self.text_buffer.get_text(start, end, False)
        text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
        try:
            html_middle = markdown.markdown(text, self.default_extensions)
        except:
            try:
                html_middle = markdown.markdown(text, extensions =self.safe_extensions)
            except:
                html_middle = markdown.markdown(text)
        html = html_middle
        self.save_html(html)

    def save_html(self, data):
        html = data
        chooser = Gtk.FileChooserDialog("Export HTML", None, Gtk.FileChooserAction.SAVE,
                                        (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                         Gtk.STOCK_OK, Gtk.ResponseType.OK))
        html_filter = Gtk.FileFilter()
        html_filter.set_name("HTML Files")
        html_filter.add_pattern("*.html")
        html_filter.add_pattern("*.htm")
        chooser.set_do_overwrite_confirmation(True)
        chooser.add_filter(html_filter)
        response = chooser.run()
        if response == Gtk.ResponseType.OK:
            file_name = chooser.get_filename()
            if not file_name.endswith(".html"):
                file_name += ".html"
            file = open(file_name, 'w')
            soup = BeautifulSoup(html)
            file.write(soup.prettify())
            file.close()
        elif response == Gtk.ResponseType.CANCEL:
            pass
        chooser.destroy()
        self.window.set_sensitive(True)

    def on_menuitem_export_pdf_activate(self, widget):
        self.window.set_sensitive(False)
        start, end = self.text_buffer.get_bounds()
        text = self.text_buffer.get_text(start, end, False)
        text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
        try:
            html_middle = markdown.markdown(text, self.default_extensions)
        except:
            try:
                html_middle = markdown.markdown(text, self.safe_extensions)
            except:
                html_middle = markdown.markdown(text)
        html = self.default_html_start + html_middle + self.default_html_end
        self.save_pdf(html)

    def on_menuitem_export_pdf_plain_activate(self, widget):
        self.window.set_sensitive(False)
        start, end = self.text_buffer.get_bounds()
        text = self.text_buffer.get_text(start, end, False)
        text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
        try:
            html_middle = markdown.markdown(text, self.default_extensions)
        except:
            try:
                html_middle = markdown.markdown(text, self.safe_extensions)
            except:
                html_middle = markdown.markdown(text)
        html = html_middle
        self.save_pdf(html)
        
    def save_pdf(self, data):
        html = data
        chooser = Gtk.FileChooserDialog("Export PDF", None, Gtk.FileChooserAction.SAVE,
                                        (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                         Gtk.STOCK_OK, Gtk.ResponseType.OK))
        pdf_filter = Gtk.FileFilter()
        pdf_filter.add_pattern("*.pdf")
        pdf_filter.set_name("PDF Files")
        chooser.add_filter(pdf_filter)
        chooser.set_do_overwrite_confirmation(True)
        response = chooser.run()
        if response == Gtk.ResponseType.OK:
            file_name = chooser.get_filename()
            if not file_name.endswith(".pdf"):
                file_name += ".pdf"
            try:
                pdfkit.from_string(html, file_name, options= {'quiet': '', 'page-size': 'Letter',
                    'margin-top': '0.75in',
                    'margin-right': '0.75in',
                    'margin-bottom': '0.75in',
                    'margin-left': '0.75in',
                    'encoding': "UTF-8",
                    'javascript-delay' : '550',
                    'no-outline': None})
            except:
                try:
                    #Failed so try with no options
                    pdfkit.from_string(html, file_name)
                except:
                    #Pdf Export failed, show warning message
                    if not self.pdf_error_warning:
                        self.pdf_error_warning = True
                        print("\nRemarkable Error:\tPDF Export Failed!!")

                    pdf_fail_dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                            Gtk.ButtonsType.CANCEL, "PDF EXPORT FAILED")
                    pdf_fail_dialog.format_secondary_text(
                            "File export to PDF was unsuccessful.")
                    pdf_fail_dialog.run()
                    pdf_fail_dialog.destroy()
        elif response == Gtk.ResponseType.CANCEL:
            pass

        chooser.destroy()
        self.window.set_sensitive(True)


    def on_menuitem_quit_activate(self, widget):
        self.clean_up()
        self.window_delete_event(self)

    def window_delete_event(self, widget, callback=None):
        start, end = self.text_buffer.get_bounds()
        text = self.text_buffer.get_text(start, end, False)
        if len(text) > 0:
            if self.check_for_save(None):
                self.save(self)
        self.quit_requested(None)

    def quit_requested(self, widget, callback_data=None):
        self.clean_up() #Second time, just to be safe
        Gtk.main_quit()

    def on_menuitem_undo_activate(self, widget):
        self.undo(self)

    def on_toolbutton_undo_clicked(self, widget):
        self.undo(self)

    def undo(self, widget):
        if self.text_buffer.can_undo:
            self.text_buffer.undo()

    def on_menuitem_redo_activate(self, widget):
        self.redo(self)

    def on_toolbutton_redo_clicked(self, widget):
        self.redo(self)

    def on_toolbutton_zoom_in_clicked(self, widget):
        self.live_preview.zoom_in()
        self.scrollPreviewToFix(self)

    def on_toolbutton_zoom_out_clicked(self, widget):
        self.live_preview.zoom_out()
        self.scrollPreviewToFix(self)

    def redo(self, widget):
        if self.text_buffer.can_redo:
            self.text_buffer.redo()

    def on_menuitem_cut_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            text = self.text_buffer.get_text(start, end, True)
            self.clipboard.set_text(text, -1)
            self.text_buffer.delete(start, end)

    def on_menuitem_copy_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            text = self.text_buffer.get_text(start, end, True)
            self.clipboard.set_text(text, -1)

    def on_menuitem_paste_activate(self, widget):
        text = self.clipboard.wait_for_text()
        if text != None:
            if self.text_buffer.get_has_selection():
                start, end = self.text_buffer.get_selection_bounds()
                self.text_buffer.delete(start, end)
            self.text_buffer.insert_at_cursor(text)

    def on_menuitem_lower_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            text = self.text_buffer.get_text(start, end, True)
            text = text.lower()
            self.text_buffer.delete(start, end)
            self.text_buffer.insert_at_cursor(text)

    def on_menuitem_title_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            text = self.text_buffer.get_text(start, end, True)
            text = text.title()
            self.text_buffer.delete(start, end)
            self.text_buffer.insert_at_cursor(text)

    def on_menuitem_upper_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            text = self.text_buffer.get_text(start, end, True)
            text = text.upper()
            self.text_buffer.delete(start, end)
            self.text_buffer.insert_at_cursor(text)

    def on_menuitem_copy_all_activate(self, widget):
        #Select the text
        text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
        try:
            text = markdown.markdown(text, self.default_extensions)
        except:
            try:
                html_middle = markdown.markdown(text, extensions =self.safe_extensions)
            except:
                html_middle = markdown.markdown(text)
        self.clipboard.set_text(text, -1)

    def on_menuitem_copy_selection_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            text = self.text_buffer.get_text(start, end, True)
            try:
                text = markdown.markdown(text, self.default_extensions)
            except:
                try:
                    html_middle = markdown.markdown(text, extensions =self.safe_extensions)
                except:
                    html_middle = markdown.markdown(text)
            self.clipboard.set_text(text, -1)

    def on_menuitem_word_wrap_activate(self, widget):
        if self.builder.get_object("menuitem_word_wrap").get_active():
            self.text_view.set_wrap_mode(Gtk.WrapMode.WORD)
            self.remarkable_settings['word-wrap'] = True
        else:
            self.text_view.set_wrap_mode(Gtk.WrapMode.NONE)
            self.remarkable_settings['word-wrap'] = False   
        self.write_settings()

    def on_menuitem_live_preview_activate(self, widget):
        self.toggle_live_preview(self)

    def toggle_live_preview(self, widget):
        if self.live_preview.get_visible():
                #hide the live preview
                self.paned.remove(self.scrolledwindow_live_preview)
                self.live_preview.set_visible(False)
                self.builder.get_object("menuitem_swap").set_sensitive(False)
                self.builder.get_object("menuitem_swap").set_tooltip_text("Enable Live Preview First")
                self.builder.get_object("toolbar1").set_visible(False)
                self.remarkable_settings['live-preview'] = False
        else:  # is hidden, thus show
            if self.editor_position == 0:
                self.paned.add(self.scrolledwindow_live_preview)
            else:
                self.paned.remove(self.scrolledwindow_text_view)
                self.paned.add(self.scrolledwindow_live_preview)
                self.paned.add(self.scrolledwindow_text_view)
            self.live_preview.set_visible(True)
            self.remarkable_settings['live-preview'] = True
            self.builder.get_object("menuitem_swap").set_sensitive(True)
            self.builder.get_object("menuitem_swap").set_tooltip_text("")
            self.builder.get_object("toolbar1").set_visible(True)
            self.update_live_preview(self)
        self.write_settings()


    def on_menuitem_swap_activate(self, widget):
        if self.live_preview.get_visible():
            self.paned.remove(self.scrolledwindow_live_preview)
            self.paned.remove(self.scrolledwindow_text_view)
            if self.editor_position == 0:
                self.paned.add(self.scrolledwindow_live_preview)
                self.paned.add(self.scrolledwindow_text_view)
                self.editor_position = 1
            else:
                self.paned.add(self.scrolledwindow_text_view)
                self.paned.add(self.scrolledwindow_live_preview)
                self.editor_position = 0
        else:
            pass # do nothing as live preview is not visible

    def on_menuitem_editor_font_activate(self, widget):
        self.font_chooser = Gtk.FontSelectionDialog()
        self.font_chooser.set_preview_text("Remarkable is the best markdown editor for Linux")
        try:
            self.font_chooser.set_font_name(self.font)
        except:
            pass #Font not initialized, do nothing, continue
        self.font_chooser.connect("destroy", self.font_dialog_destroyed)
        self.font_ok_button = self.font_chooser.get_ok_button()
        self.font_ok_button.connect("clicked", self.font_dialog_ok)
        self.font_cancel_button = self.font_chooser.get_cancel_button()
        self.font_cancel_button.connect("clicked", self.font_dialog_cancel)
        self.font_chooser.show()

    def font_dialog_destroyed(self, widget):
        self.font_chooser.destroy()

    def font_dialog_cancel(self, widget):
        self.font_chooser.destroy()

    def font_dialog_ok(self, widget):
        self.font = self.font_chooser.get_font_name()
        self.remarkable_settings['font'] = self.font #Save prefs
        self.write_settings()    
        self.text_view.override_font(Pango.FontDescription(self.font))

        #Now adjust the size using TextTag

        self.font_dialog_destroyed(self)

    def on_menuitem_statusbar_activate(self, widget):
        if self.statusbar.get_visible():
            self.statusbar.set_visible(False)
            self.builder.get_object("menuitem_statusbar").set_label("Show Statusbar")
            self.remarkable_settings['statusbar'] = False
        else:
            self.statusbar.set_visible(True)
            self.update_status_bar(self)
            self.builder.get_object("menuitem_statusbar").set_label("Hide Statusbar")
            self.remarkable_settings['statusbar'] = True
        self.write_settings()

    def on_menuitem_toolbar_activate(self, widget):
        if self.toolbar.get_visible():
            self.toolbar.set_visible(False)
            self.builder.get_object("menuitem_toolbar").set_label("Show Toolbar")
            self.builder.get_object("toolbar1").set_visible(False)
            self.remarkable_settings['toolbar'] = False
            
        else:
            self.toolbar.set_visible(True)
            self.builder.get_object("menuitem_toolbar").set_label("Hide Toolbar")
            self.builder.get_object("toolbar1").set_visible(True)
            self.remarkable_settings['toolbar'] = True
        self.write_settings()

    def on_menuitem_preview_browser_activate(self, widget):
        #Create a temporary HTML file
        tf = tempfile.NamedTemporaryFile(delete = False)
        self.temp_file_list.append(tf)
        tf_name = tf.name

        text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
        try:
            html_middle = markdown.markdown(text, self.default_extensions)
        except:
            try:
                html_middle = markdown.markdown(text, extensions =self.safe_extensions)
            except:
                html_middle = markdown.markdown(text)
        html = self.default_html_start + html_middle + self.default_html_end
        tf.write(html.encode())
        tf.flush()
        webbrowser.open_new_tab(tf_name)

        #Load it in the default web browser

    def on_menuitem_night_mode_activate(self, widget):
        if self.builder.get_object("menuitem_night_mode").get_active():
            self.settings.set_property("gtk-application-prefer-dark-theme", True)
            self.remarkable_settings['nightmode'] = True
        else:
            self.settings.set_property("gtk-application-prefer-dark-theme", False)
            self.remarkable_settings['nightmode'] = False
        self.write_settings()

    def on_menuitem_fullscreen_activate(self, widget):
        if self.is_fullscreen:
            self.window.unfullscreen()
            self.is_fullscreen = False
            self.builder.get_object("menuitem_fullscreen").set_label("Fullscreen")
        else:
            self.window.fullscreen()
            self.is_fullscreen = True
            self.builder.get_object("menuitem_fullscreen").set_label("Exit fullscreen")

    def on_menuitem_bold_activate(self, widget):
        self.bold(self)

    def on_toolbutton_bold_clicked(self, widget):
        self.bold(self)

    def bold(self, widget):
        if not self.text_buffer.get_has_selection():  # nothing has been selected, add **** and place cursor in middle
            self.text_buffer.insert_at_cursor("****")
            loc = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            loc.backward_chars(2)
            self.text_buffer.place_cursor(loc)
        else:  # turn selection bold
            selection_bounds = self.text_buffer.get_selection_bounds()
            mark1 = self.text_buffer.create_mark(None, selection_bounds[0], False)
            mark2 = self.text_buffer.create_mark(None, selection_bounds[1], False)
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark1), "**")
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark2), "**")

    def on_menuitem_italic_activate(self, widget):
        self.italic(self)

    def on_toolbutton_italic_clicked(self, widget):
        self.italic(self)

    def italic(self, widget):
        if not self.text_buffer.get_has_selection():  # nothing has been selected, add ** and place cursor in middle
            self.text_buffer.insert_at_cursor("**")
            loc = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            loc.backward_chars(1)
            self.text_buffer.place_cursor(loc)
        else:  # turn selection italic
            selection_bounds = self.text_buffer.get_selection_bounds()
            mark1 = self.text_buffer.create_mark(None, selection_bounds[0], False)
            mark2 = self.text_buffer.create_mark(None, selection_bounds[1], False)
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark1), "*")
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark2), "*")

    def on_menuitem_strikethrough_activate(self, widget):
        self.strikethrough(self)

    def on_toolbutton_strikethrough_clicked(self, widget):
        self.strikethrough(self)

    def strikethrough(self, widget):
        if not self.text_buffer.get_has_selection():  # nothing has been selected, add **** and place cursor in middle
            self.text_buffer.insert_at_cursor("~~~~")
            loc = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            loc.backward_chars(2)
            self.text_buffer.place_cursor(loc)
        else:  # strikethrough selection
            selection_bounds = self.text_buffer.get_selection_bounds()
            mark1 = self.text_buffer.create_mark(None, selection_bounds[0], False)
            mark2 = self.text_buffer.create_mark(None, selection_bounds[1], False)
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark1), "~~")
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark2), "~~")

    def on_menuitem_highlight_activate(self, widget):
        if not self.text_buffer.get_has_selection():  # nothing has been selected, add ==== and place cursor in middle
            self.text_buffer.insert_at_cursor("====")
            loc = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            loc.backward_chars(2)
            self.text_buffer.place_cursor(loc)
        else:  # highlight the selected text
            selection_bounds = self.text_buffer.get_selection_bounds()
            mark1 = self.text_buffer.create_mark(None, selection_bounds[0], False)
            mark2 = self.text_buffer.create_mark(None, selection_bounds[1], False)
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark1), "==")
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark2), "==")

    def on_menuitem_superscript_activate(self, widget):
        if not self.text_buffer.get_has_selection():  # nothing has been selected, add ^^ and place cursor in middle
            self.text_buffer.insert_at_cursor("^^")
            loc = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            loc.backward_chars(1)
            self.text_buffer.place_cursor(loc)
        else:  # convert selection to superscript
            selection_bounds = self.text_buffer.get_selection_bounds()
            mark1 = self.text_buffer.create_mark(None, selection_bounds[0], False)
            mark2 = self.text_buffer.create_mark(None, selection_bounds[1], False)
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark1), "^")
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark2), "^")

    def on_menuitem_subscript_activate(self, widget):
        if not self.text_buffer.get_has_selection():  # nothing has been selected, add ~~ and place cursor in middle
            self.text_buffer.insert_at_cursor("~~")
            loc = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            loc.backward_chars(1)
            self.text_buffer.place_cursor(loc)
        else:  # convert selection to subscript
            selection_bounds = self.text_buffer.get_selection_bounds()
            mark1 = self.text_buffer.create_mark(None, selection_bounds[0], False)
            mark2 = self.text_buffer.create_mark(None, selection_bounds[1], False)
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark1), "~")
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(mark2), "~")

    def on_menuitem_block_quote_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()

            start_line = start.get_line()
            end_line = end.get_line()

            while start_line <= end_line:
                temp_iter = self.text_buffer.get_iter_at_line(start_line)
                self.text_buffer.insert(temp_iter, ">")
                start_line += 1
        else:
            temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            line_number = temp_iter.get_line()
            start_iter = self.text_buffer.get_iter_at_line(line_number)
            self.text_buffer.insert(start_iter, ">")

    def on_menuitem_code_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()

            start_line = start.get_line()
            end_line = end.get_line()

            while (start_line <= end_line):
                temp_iter = self.text_buffer.get_iter_at_line(start_line)
                self.text_buffer.insert(temp_iter, "\t")
                start_line += 1
        else:
            temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            line_number = temp_iter.get_line()
            start_iter = self.text_buffer.get_iter_at_line(line_number)
            self.text_buffer.insert(start_iter, "\t")

    def on_menuitem_bullet_list_activate(self, widget):
        if self.text_buffer.get_has_selection():
            start, end = self.text_buffer.get_selection_bounds()
            start_line = start.get_line()
            end_line = end.get_line()

            while (start_line <= end_line):
                temp_iter = self.text_buffer.get_iter_at_line(start_line)
                self.text_buffer.insert(temp_iter, "- ")
                start_line += 1

        else:
            temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
            line_number = temp_iter.get_line()
            start_iter = self.text_buffer.get_iter_at_line(line_number)
            self.text_buffer.insert(start_iter, "- ")

    def on_menuitem_heading_1_activate(self, widget):
        temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
        line_number = temp_iter.get_line()
        start_iter = self.text_buffer.get_iter_at_line(line_number)
        self.text_buffer.insert(start_iter, "#")

    def on_menuitem_heading_2_activate(self, widget):
        temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
        line_number = temp_iter.get_line()
        start_iter = self.text_buffer.get_iter_at_line(line_number)
        self.text_buffer.insert(start_iter, "##")

    def on_menuitem_heading_3_activate(self, widget):
        temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
        line_number = temp_iter.get_line()
        start_iter = self.text_buffer.get_iter_at_line(line_number)
        self.text_buffer.insert(start_iter, "###")

    def on_menuitem_heading_4_activate(self, widget):
        temp_iter = self.text_buffer.get_iter_at_mark(self.text_buffer.get_insert())
        line_number = temp_iter.get_line()
        start_iter = self.text_buffer.get_iter_at_line(line_number)
        self.text_buffer.insert(start_iter, "####")

    def on_menuitem_horizonatal_rule_activate(self, widget):
        if not self.text_buffer.get_has_selection():
            self.text_buffer.insert_at_cursor("\n***\n")
        else:  # turn selection bold
            selection_bounds = self.text_buffer.get_selection_bounds()
            markR = self.text_buffer.create_mark(None, selection_bounds[1], False)
            self.text_buffer.insert(self.text_buffer.get_iter_at_mark(markR), "\n***\n")

    def on_menuitem_timestamp_activate(self, widget):
        self.insert_timestamp(self)

    def on_toolbutton_timestamp_clicked(self, widget):
        self.insert_timestamp(self)

    def insert_timestamp(self, widget):
        text = datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p") + " "
        self.text_buffer.insert_at_cursor(text + "\n")
        self.text_view.grab_focus()

    def on_menuitem_image_activate(self, widget):
        self.insert_image(self)

    def on_toolbutton_image_clicked(self, widget):
        self.insert_image(self)

    def insert_image(self, widget):
        self.insert_window_image = Gtk.Window()
        self.insert_window_image.set_title("Insert Image")
        self.insert_window_image.set_resizable(True)
        self.insert_window_image.set_border_width(6)
        self.insert_window_image.set_default_size(300, 250)
        self.insert_window_image.set_position(Gtk.WindowPosition.CENTER)
        vbox = Gtk.VBox()
        label_alt_text = Gtk.Label("Alt Text:")
        self.entry_alt_text_i = Gtk.Entry()
        label_title = Gtk.Label("Title:")
        self.entry_title_i = Gtk.Entry()
        label_url = Gtk.Label("Path/Url:")
        self.entry_url_i = Gtk.Entry()
        vbox.pack_start(label_alt_text, self, False, False)
        vbox.pack_start(self.entry_alt_text_i, self, False, False)
        vbox.pack_start(label_title, self, False, False)
        vbox.pack_start(self.entry_title_i, self, False, False)
        vbox.pack_start(label_url, self, False, False)
        self.hbox_url = Gtk.HBox()
        self.hbox_url.pack_start(self.entry_url_i, self, True, False)
        self.path_file_button = Gtk.FileChooserButton(title= "Select an image")
        self.path_file_button.connect("file-set", self.file_chooser_button_clicked)
        self.hbox_url.pack_start(self.path_file_button, self, False, False)
        vbox.pack_start(self.hbox_url, self, False, False)
        button = Gtk.Button("Insert Image")
        vbox.pack_end(button, self, False, False)
        self.insert_window_image.add(vbox)
        self.insert_window_image.show_all()
        button.connect("clicked", self.insert_image_cmd, self.insert_window_image)

    def file_chooser_button_clicked(self, widget):
        filePath = widget.get_filename()
        self.entry_url_i.set_text(filePath)

    def insert_image_cmd(self, widget, window):
        if self.entry_url_i.get_text():
            if self.entry_title_i.get_text():
                if self.entry_alt_text_i.get_text():  # fill alt_text with a space ( > 1 char required)
                    link = "![" + self.entry_alt_text_i.get_text() + "](" + self.entry_url_i.get_text()
                    link += '  "' + self.entry_title_i.get_text() + '")'
                    self.text_buffer.insert_at_cursor(link)
                else:
                    link = "![ ](" + self.entry_url_i.get_text()
                    link += '  "' + self.entry_title_i.get_text() + '")'
                    self.text_buffer.insert_at_cursor(link)
            else:
                link = "![" + self.entry_alt_text_i.get_text() + "](" + self.entry_url_i.get_text() + ") "
                self.text_buffer.insert_at_cursor(link)
        else:
            pass
        self.insert_window_image.hide()

    def on_menuitem_link_activate(self, widget):
        self.insert_link(self)

    def on_toolbutton_link_clicked(self, widget):
        self.insert_link(self)

    def insert_link(self, widget):
        self.insert_window_link = Gtk.Window()
        self.insert_window_link.set_title("Insert Link")
        self.insert_window_link.set_resizable(True)
        self.insert_window_link.set_border_width(6)
        self.insert_window_link.set_default_size(350, 250)
        self.insert_window_link.set_position(Gtk.WindowPosition.CENTER)
        vbox = Gtk.VBox()
        label_alt_text = Gtk.Label("Alt Text:")
        self.entry_alt_text = Gtk.Entry()
        label_url = Gtk.Label("Url:")
        self.entry_url = Gtk.Entry()
        vbox.pack_start(label_alt_text, self, False, False)
        vbox.pack_start(self.entry_alt_text, self, False, False)
        vbox.pack_start(label_url, self, False, False)
        vbox.pack_start(self.entry_url, self, False, False)
        button = Gtk.Button("Insert Link")
        vbox.pack_end(button, self, False, False)
        self.insert_window_link.add(vbox)
        self.insert_window_link.show_all()
        button.connect("clicked", self.insert_link_cmd, self.insert_window_link)

    def insert_link_cmd(self, widget, window):
        if self.entry_url.get_text():
            link = "[" + self.entry_alt_text.get_text() + "](" + self.entry_url.get_text() + ") "
            self.text_buffer.insert_at_cursor(link)
        else:
            pass
        self.insert_window_link.hide()


    #styles
    def update_style(self, widget):
        self.default_html_start = '<!doctype HTML><html><head><meta charset="utf-8"><title>Made with Remarkable!</title><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.1/styles/github.min.css">'
        self.default_html_start += "<style type='text/css'>" + self.css + "</style>" ##Change this to default style
        self.default_html_start += "</head><body>"

    def on_menuitem_dark_activate(self, widget):
        self.css = self.dark
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "dark"
        self.write_settings()

    def on_menuitem_foghorn_activate(self, widget):
        self.css = self.foghorn
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "foghorn"
        self.write_settings()

    def on_menuitem_github_activate(self, widget):
        self.css = self.github
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "github"
        self.write_settings()

    def on_menuitem_handwritten_activate(self, widget):
        self.css = self.handwriting_css
        self.update_style(self)
        self.update_live_preview(self)
        self.live_preview.zoom_in()
        self.remarkable_settings['style'] = "handwriting_css"
        self.write_settings()

    def on_menuitem_markdown_activate(self, widget):
        self.css = self.markdown
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "markdown"
        self.write_settings()

    def on_menuitem_metro_vibes_activate(self, widget):
        self.css = self.metro_vibes
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "metro_vibes"
        self.write_settings()

    def on_menuitem_metro_vibes_dark_activate(self, widget):
        self.css = self.metro_vibes_dark
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "metro_vibes_dark"
        self.write_settings()


    def on_menuitem_modern_activate(self, widget):
        self.css = self.modern_css
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "modern_css"
        self.write_settings()

    def on_menuitem_screen_activate(self, widget):
        self.css = self.screen
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "screen"
        self.write_settings()
    
    def on_menuitem_solarized_dark_activate(self, widget):
        self.css = self.solarized_dark
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "solarized_dark"
        self.write_settings()

    def on_menuitem_solarized_light_activate(self, widget):
        self.css = self.solarized_light
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "solarized_light"
        self.write_settings()

    ##Custom CSS
    def on_menuitem_custom_activate(self, widget):
        self.custom_window = Gtk.Window()
        self.custom_window.set_default_size(640, 480)
        self.custom_window.set_position(Gtk.WindowPosition.CENTER)
        self.custom_window.set_title("Custom CSS")

        self.custom_vbox = Gtk.VBox()
        self.custom_scroller = Gtk.ScrolledWindow()
        self.custom_button = Gtk.Button("Apply")
        self.custom_vbox.pack_end(self.custom_button, False, False, 0)
        self.custom_text_view = Gtk.TextView()
        self.custom_text_buffer = Gtk.TextBuffer()
        self.custom_text_buffer.set_text(self.custom_css)
        self.custom_text_view.set_buffer(self.custom_text_buffer)
        self.custom_scroller.add(self.custom_text_view)
        self.custom_vbox.pack_start(self.custom_scroller, True, True, 0)
        self.custom_window.add(self.custom_vbox)
        self.custom_window.show_all()
        self.custom_button.connect("clicked", self.apply_custom_css, self.custom_window, self.custom_text_buffer)

    def apply_custom_css(self, widget, window, tb):
        start, end = tb.get_bounds()
        self.custom_css = tb.get_text(start, end, False).replace("'", '"')
        self.css = self.custom_css
        self.remarkable_settings['css'] = self.css
        window.hide()
        self.update_style(self)
        self.update_live_preview(self)
        self.remarkable_settings['style'] = "custom"
        self.write_settings()
    ##End Custom CSS

    def on_menuitem_reportbug_activate(self, widget):
        bug_report = Gtk.Window()
        bug_report.set_title("Bug Report")
        bug_report.set_default_size(640,480)
        bug_report.set_position(Gtk.WindowPosition.CENTER)
        bug_browser = WebKit.WebView()
        bug_browser.connect("console-message", self._javascript_console_message) # Suppress .js output
        bug_scroller = Gtk.ScrolledWindow()
        bug_scroller.add(bug_browser)
        bug_report.add(bug_scroller)
        bug_browser.open("https://docs.google.com/forms/d/163o0Tyrzo1JsD4sFntDMcIFLGxy9GbUHwEV7I8HwBcM/viewform?usp=send_form")
        bug_report.show_all()

    def on_menuitem_feedback_activate(self, widget):
        window_feedback = Gtk.Window()
        window_feedback.set_title("Feedback Form")
        window_feedback.set_default_size(640, 640)
        window_feedback.set_position(Gtk.WindowPosition.CENTER)
        feedback_browser = WebKit.WebView()
        feedback_browser.connect("console-message", self._javascript_console_message) # Suppress .js output
        feedback_scroller = Gtk.ScrolledWindow()
        feedback_scroller.add(feedback_browser)
        window_feedback.add(feedback_scroller)
        feedback_browser.open("https://jamiemcgowan.typeform.com/to/ng5Lhc")
        window_feedback.show_all()

    def on_menuitem_about_activate(self, widget):
        self.AboutDialog.show(self)

    def on_menuitem_homepage_activate(self, widget):
        webbrowser.open_new_tab("http://remarkableapp.github.io")

    def on_menuitem_donate_activate(self, widget):
        webbrowser.open_new_tab("http://remarkableapp.github.io/linux/donate")

    def on_menuitem_check_for_updates_activate(self, widget):
        _thread.start_new_thread(self.check_for_updates, (True,))

    def check_for_updates(self, show = False):
        try:
            update_check = urlopen("http://remarkableapp.github.io/latest")
            latest_version = float(update_check.readline())
            if app_version < latest_version:
                print("There is a new version avaiable")
                subprocess.Popen(['notify-send', "Remarkable: A new version of this app is avaiable"])
                update_check = urlopen("http://remarkableapp.github.io/change_log")
                md = update_check.read()
                html = markdown.markdown(md)
                if show:
                    webbrowser.open_new_tab("http://remarkableapp.github.io")
            else:
                if show:
                    subprocess.Popen(['notify-send', "Remarkable: You already have the latest version of this app available"])
                    print("You have the latest version of this app available")
        except:
            print("Warning: Remarkable could not connect to the internet to check for updates")

    def on_text_view_changed(self, widget):
        start, end = self.text_buffer.get_bounds()
        if self.text_buffer.can_undo:
            self.builder.get_object("menuitem_undo").set_sensitive(True)
            self.builder.get_object("toolbutton_undo").set_sensitive(True)

        else:
            self.builder.get_object("menuitem_undo").set_sensitive(False)
            self.builder.get_object("toolbutton_undo").set_sensitive(False)

        if self.text_buffer.can_redo:
            self.builder.get_object("menuitem_redo").set_sensitive(True)
            self.builder.get_object("toolbutton_redo").set_sensitive(True)
        else:
            self.builder.get_object("menuitem_redo").set_sensitive(False)
            self.builder.get_object("toolbutton_redo").set_sensitive(False)

        if self.statusbar.get_visible():
            self.update_status_bar(self)
        else:  # statusbar not present, don't need to update/calculate
            pass
        if self.live_preview.get_visible():
            self.update_live_preview(self)
            self.scrollPreviewTo(self)

        else:  # live preview not enabled, don't need to update the view
            pass

        #Update  title to reflect changes
        if self.text_buffer.get_modified():
            title = self.window.get_title()
            if title[0] != "*":
                title = "*" + title
                self.window.set_title(title)


    def update_status_bar(self, widget):
        self.statusbar.pop(self.context_id)
        lines = self.text_buffer.get_line_count()
        chars = self.text_buffer.get_char_count()
        words = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False).split()
        word_count = 0
        word_exceptions = ["#", "##", "###", "####", "#####", "######", "*", "**", "-", "+", "_", "/", "\\", "/", ":",
                           ";", "@", "'", "~", "(", ")", "[", "]", "{", "}", "((", "))", "+-", "-+", "/=", ".", "|",
                           "!", "!!", "!!!", "$", "", "%", "^", "&"]  # exclude these from word count
        for w in words:
            if w not in word_exceptions:
                if not re.match('^[0-9]{1,3}$', w):
                    word_count += 1
        self.status_message = "Lines: " + str(lines) + ", " + "Words: " + str(word_count) + ", Characters: " + str(chars)
        self.statusbar.push(self.context_id, self.status_message)

    def update_live_preview(self, widet):
        text = self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False)
        try:
            html_middle = markdown.markdown(text, self.default_extensions)
        except:
            try:
                html_middle = markdown.markdown(text, extensions =self.safe_extensions)
            except:
                html_middle = markdown.markdown(text)
        html = self.default_html_start + html_middle + self.default_html_end
        self.live_preview.load_string(html, "text/html", "utf-8", "file://") #Thanks to 

    """
        This function suppresses the messages from the WebKit (live preview) console
    """
    def _javascript_console_message(self, view, message, line, sourceid):
        return True


    """
        This function deletes any temporary files that were created during execution
    """
    def clean_up(self):
        i = len(self.temp_file_list) - 1
        while i >= 0:
            os.remove(self.temp_file_list[0].name)
            del self.temp_file_list[0]
            i -= 1
